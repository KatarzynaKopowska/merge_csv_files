
#this is a merge function which concatenates data from .csv files
#and returns final list of dictionarieswith specific data


import csv


def merge(persons_file, visits_file):
    #open and read files
    o_persons_file = open(persons_file)
    r_persons_file = list(csv.reader(o_persons_file))

    o_visits_file = open(visits_file)
    r_visits_file = list(csv.reader(o_visits_file))

    # create a list of person's id from visits_file.csv and id_list
    # from persons_file.csv to get number of visits
    persons_id_list = [(r_visits_file[i][1]) for i, num in enumerate(r_visits_file)]
    id_list = [(r_persons_file[i][0]) for i, num in enumerate(r_persons_file)]

    # count visits
    number_of_visits = []
    for id_number in id_list:
        counter = persons_id_list.count(id_number)
        number_of_visits.append(counter)

    # create final list of dictionaries with merged data
    final_list = []
    index_num = []
    for index_num in range(len(id_list)):
        final_dict = {}
        final_dict["id"] = id_list[index_num]
        final_dict["name"] = r_persons_file[index_num][1]
        final_dict["surname"] = r_persons_file[index_num][2]
        final_dict["visits"] = number_of_visits[index_num]
        index_num+=1
        final_list.append(final_dict)
    return final_list

print(merge('./persons_file.csv', '/visits_file.csv'))
