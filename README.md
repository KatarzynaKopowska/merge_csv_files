This is one of my solutions for an internship task.
The goal was to create a function that merge selected data from two .csv files,
returning a list of dictionaries.

I use standard csv library to open and read .csv files and list comprehension to
process data.

To run function user needs two arguments - files paths.
